<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_name"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            /*$logout_date   = date('Y/m/d H:i:s');
            $empid=$_SESSION["user_empid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where cntry_code='".$_SESSION["user_cc"]."' and mobile_num='".$_SESSION["user_mob"]."'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));*/

            unset($_SESSION["user_name"]);
            //unset($_SESSION["user_cc"]);
            //unset($_SESSION["user_mob"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CIMS :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 mb-1">
        <div class="col-10 offset-1 text-right">
            Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a>
        </div>
    </div>
    <div class="row video-panel mt-2">
        <div class="col-12 col-lg-8">
            <div class="embed-responsive embed-responsive-16by9" id="webcast">
              <iframe class="embed-responsive-item" src="video.php" allowfullscreen scrolling="no"></iframe>
            </div>    
        </div>
        <div class="col-12 col-lg-4">
            <div class="question-box">
              <form id="question-form" method="post" role="form">
                      <div class="row">
                        <div class="col-10 offset-1">
                            <h6>Ask your Question:</h6>
                            <div id="message"></div>
                            <div class="form-group">
                               <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="6"></textarea>
                            </div>
                        </div>
                      </div>
                      <div class="row mt-3">
                        <div class="col-10 offset-1">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <!--<input type="hidden" id="user_cc" name="user_cc" value="<?php echo $_SESSION['user_cc']; ?>">
                          <input type="hidden" id="user_mob" name="user_mob" value="<?php echo $_SESSION['user_mob']; ?>">-->
                            <input type="submit" class="btn btn-block btn-outline-warning" value="Submit Question" alt="Submit">
                            
                        </div>
                      </div>  
                </form>
            </div>    
        </div>
        
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

</script>
</body>
</html>