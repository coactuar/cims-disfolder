<?php
require_once "config.php";
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CIMS :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row content">
        <div class="col-8 col-md-6 col-lg-4 offset-md-3 offset-lg-4 offset-2">
            <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                  </div>
                  
                  <!--<div class="input-group">
                    <select id="country" name="country" class="form-control" required>
                    <option value="-1">Select Country Code</option>
                    <?php
                    $query="SELECT * FROM tbl_countries order by country asc";
                    $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
                    while($data = mysqli_fetch_assoc($res))
                    {
                        
                        $country = ($data['country']);
                        if($country !='')
                        {
                     ?>
                     <option value="<?php echo $data['cntry_code']; ?>"><?php echo $country.'(+'.$data['cntry_code'].')'; ?></option>
                     <?php
                        }
                    }
                    ?>
                </select>
                  </div>-->
                  <!--<div class="input-group">
                    <input type="number" class="form-control" placeholder="Mobile No." aria-label="Location" aria-describedby="basic-addon1" name="mobNum" id="mobNum" required>
                  </div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Location" aria-label="Location" aria-describedby="basic-addon1" name="loc" id="loc" required>
                  </div>-->
                  
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Login</button>
                  </div>
                
            </form>        
        </div>
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
    var cc= $('#country').val();
    if(cc == "-1")
    {
        alert('Select Country');
        return false;
    }
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
</body>
</html>