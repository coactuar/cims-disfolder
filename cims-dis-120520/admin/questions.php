<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["admin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["admin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Questions</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    
     <div class="row login-info links">   
        <div class="col-8 text-left">
            <a href="users.php">Users</a> | <a href="questions.php">Questions</a>
        </div>
        <div class="col-4 text-right">
            <a href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> <a href="?action=logout">Logout</a>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <a href="export_questions.php"><img src="excel.png" height="45" alt=""/></a>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="questions"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
getQues('1');
});

function update(pageNum)
{
  getQues(pageNum);
}

function getQues(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getquestions', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#questions").html(response);
            
        }
    });
    
}

function getupdate()
{
	var curr = $('#ques_count').text();
	//alert(curr);
	
	$.ajax({ url: 'ajax.php',
         data: {action: 'getquesupdate'},
         type: 'post',
         success: function(output) {
					 var msg = output;
                     
					 if (Number(output) > Number(curr))
					 {
						 newmsg = Number(output) - Number(curr);
						 if(newmsg == "1")
						 { 
						 msg = newmsg+ " new question available.";
						 }
						 else{
						 msg = newmsg+ " new questions available.";
						 }
						 msg += " <a href='questions.php'>Refresh</a>";
					 }
					 else
					 {
						 msg="";
					 }
					 
					 	$("#ques_update").html(msg);
					 
                  }
});
}

setInterval(function(){ getupdate(); }, 10000);

function updateQues(id)
{
    var qid = '#answer'+id;
    var curval = $(qid).val(); 
    $.ajax({
        url: 'ajax.php?id=' + id,
         data: {action: 'updateques', val: curval },
         type: 'post',
         success: function(output) {
             //alert(output);
             getQues('1');
         }
   });   
}

function updSpk(qid, spk)
{
    $.ajax({
        url: 'ajax.php?',
         data: {action: 'updatespk', ques:qid, val: spk },
         type: 'post',
         success: function(output) {
             getQues('1');
         }
   });   
    
}
function updSpkAns(qid, ans)
{
    $.ajax({
        url: 'ajax.php?',
         data: {action: 'updatespkans', ques:qid, val: ans },
         type: 'post',
         success: function(output) {
             getQues('1');
         }
   });   
    
}


</script>

</body>
</html>