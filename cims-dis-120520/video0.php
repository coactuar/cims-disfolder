<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#videoPlayer{
    width:100%;
    height:100%;
}
</style>

</head>

<body>
<!-- CSS -->
<link href="https://vjs.zencdn.net/7.2.3/video-js.css" rel="stylesheet">
<!-- HTML -->
<video id='videoPlayer' class="video-js vjs-default-skin" width="400" height="300" controls>
<source type="application/x-mpegURL" src="https://d2tolx7e4cz3cw.cloudfront.net/najm/live.m3u8">
</video>
<!-- JS code -->
<!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
<script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>
<script src="https://vjs.zencdn.net/7.2.3/video.js"></script>
<script>
var player = videojs('videoPlayer');
//player.play();
</script>
</body>
</html>