-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 12:43 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cims`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `country` varchar(43) DEFAULT NULL,
  `cntry_code` varchar(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`country`, `cntry_code`) VALUES
('Afghanistan', '93'),
('Albania', '355'),
('Algeria', '213'),
('American Samoa', '1-684'),
('Andorra', '376'),
('Angola', '244'),
('Anguilla', '1-264'),
('Antarctica', '672'),
('Antigua', '1-268'),
('Argentina', '54'),
('Armenia', '374'),
('Aruba', '297'),
('Ascension', '247'),
('Australia', '61'),
('Australian External Territories', '672'),
('Austria', '43'),
('Azerbaijan', '994'),
('Bahamas', '1-242'),
('Bahrain', '973'),
('Bangladesh', '880'),
('Barbados', '1-246'),
('Barbuda', '1-268'),
('Belarus', '375'),
('Belgium', '32'),
('Belize', '501'),
('Benin', '229'),
('Bermuda', '1-441'),
('Bhutan', '975'),
('Bolivia', '591'),
('Bosnia & Herzegovina', '387'),
('Botswana', '267'),
('Brazil', '55'),
('British Virgin Islands', '1-284'),
('Brunei Darussalam', '673'),
('Bulgaria', '359'),
('Burkina Faso', '226'),
('Burundi', '257'),
('Cambodia', '855'),
('Cameroon', '237'),
('Canada', '1'),
('Cape Verde Islands', '238'),
('Cayman Islands', '1-345'),
('Central African Republic', '236'),
('Chad', '235'),
('Chatham Island (New Zealand)', '64'),
('Chile', '56'),
('China (PRC)', '86'),
('Christmas Island', '61-8'),
('', ''),
('Cocos-Keeling Islands', '61'),
('Colombia', '57'),
('Comoros', '269'),
('Congo', '242'),
('Congo, Dem. Rep. of?', '243'),
('Cook Islands', '682'),
('Costa Rica', '506'),
('C?te d\'Ivoire', '225'),
('Croatia', '385'),
('Cuba', '53'),
('Cuba', '5399'),
('Cura?ao', '599'),
('Cyprus', '357'),
('Czech Republic', '420'),
('Denmark', '45'),
('Diego Garcia', '246'),
('Djibouti', '253'),
('Dominica', '1-767'),
('Dominican Republic', '1-809'),
('East Timor', '670'),
('Easter Island', '56'),
('Ecuador', '593'),
('Egypt', '20'),
('El Salvador', '503'),
('Equatorial Guinea', '240'),
('Eritrea', '291'),
('Estonia', '372'),
('Ethiopia', '251'),
('Falkland Islands', '500'),
('Faroe Islands', '298'),
('Fiji Islands', '679'),
('Finland', '358'),
('France', '33'),
('French Antilles', '596'),
('French Guiana', '594'),
('French Polynesia', '689'),
('Gabonese Republic', '241'),
('Gambia', '220'),
('Georgia', '995'),
('Germany', '49'),
('Ghana', '233'),
('Gibraltar', '350'),
('Greece', '30'),
('Greenland', '299'),
('Grenada', '1-473'),
('Guadeloupe', '590'),
('Guam', '1-671'),
('Guantanamo Bay', '5399'),
('Guatemala', '502'),
('Guinea-Bissau', '245'),
('Guinea', '224'),
('Guyana', '592'),
('Haiti', '509'),
('Honduras', '504'),
('Hong Kong', '852'),
('Hungary', '36'),
('Iceland', '354'),
('India', '91'),
('Indonesia', '62'),
('Inmarsat (Atlantic Ocean - East)', '871'),
('Inmarsat (Atlantic Ocean - West)', '874'),
('Inmarsat (Indian Ocean)', '873'),
('Inmarsat (Pacific Ocean)', '872'),
('International Freephone Service', '800'),
('International Shared Cost Service (ISCS)', '808'),
('Iran', '98'),
('Iraq', '964'),
('Ireland', '353'),
('Iridium', '8816'),
('Israel', '972'),
('Italy', '39'),
('Jamaica', '1-876'),
('Japan', '81'),
('Jordan', '962'),
('Kazakhstan', '7'),
('Kenya', '254'),
('Kiribati', '686'),
('Korea (North)', '850'),
('Korea (South)', '82'),
('Kuwait', '965'),
('Kyrgyz Republic', '996'),
('Laos', '856'),
('Latvia', '371'),
('Lebanon', '961'),
('Lesotho', '266'),
('Liberia', '231'),
('Libya', '218'),
('Liechtenstein', '423'),
('Lithuania', '370'),
('Luxembourg', '352'),
('Macao', '853'),
('Macedonia', '389'),
('Madagascar', '261'),
('Malawi', '265'),
('Malaysia', '60'),
('Maldives', '960'),
('Mali Republic', '223'),
('Malta', '356'),
('Marshall Islands', '692'),
('Martinique', '596'),
('Mauritania', '222'),
('Mauritius', '230'),
('Mayotte Island', '269'),
('Mexico', '52'),
('Micronesia, (Federal States of)', '691'),
('Midway Island', '1-808'),
('Moldova', '373'),
('Monaco', '377'),
('Mongolia', '976'),
('Montenegro', '382'),
('Montserrat', '1-664'),
('Morocco', '212'),
('Mozambique', '258'),
('Myanmar', '95'),
('Namibia', '264'),
('Nauru', '674'),
('Nepal', '977'),
('Netherlands', '31'),
('Netherlands Antilles', '599'),
('Nevis', '1-869'),
('New Caledonia', '687'),
('New Zealand', '64'),
('Nicaragua', '505'),
('Niger', '227'),
('Nigeria', '234'),
('Niue', '683'),
('Norfolk Island', '672'),
('Northern Marianas Islands', '1-670'),
('', ''),
('Norway', '47'),
('Oman', '968'),
('Pakistan', '92'),
('Palau', '680'),
('Palestinian Settlements', '970'),
('Panama', '507'),
('Papua New Guinea', '675'),
('Paraguay', '595'),
('Peru', '51'),
('Philippines', '63'),
('Poland', '48'),
('Portugal', '351'),
('Puerto Rico', '1-787'),
('Qatar', '974'),
('R?union Island', '262'),
('Romania', '40'),
('Russia', '7'),
('Rwandese Republic', '250'),
('St. Helena', '290'),
('St. Kitts/Nevis', '1-869'),
('St. Lucia', '-757'),
('St. Pierre & Miquelon', '508'),
('St. Vincent & Grenadines', '1-784'),
('Samoa', '685'),
('San Marino', '378'),
('S?o Tom? and Principe', '239'),
('Saudi Arabia', '966'),
('Senegal', '221'),
('Serbia', '381'),
('Seychelles Republic', '248'),
('Sierra Leone', '232'),
('Singapore', '65'),
('Slovak Republic', '421'),
('Slovenia', '386'),
('Solomon Islands', '677'),
('Somali Democratic Republic', '252'),
('South Africa', '27'),
('Spain', '34'),
('Sri Lanka', '94'),
('Sudan', '249'),
('Suriname', '597'),
('Swaziland', '268'),
('Sweden', '46'),
('Switzerland', '41'),
('Syria', '963'),
('Taiwan', '886'),
('Tajikistan', '992'),
('Tanzania', '255'),
('Thailand', '66'),
('Thuraya (Mobile Satellite service)', '88216'),
('Timor Leste', '670'),
('Togolese Republic', '228'),
('Tokelau', '690'),
('Tonga Islands', '676'),
('Trinidad & Tobago', '1-868'),
('Tunisia', '216'),
('Turkey', '90'),
('Turkmenistan', '993'),
('Turks and Caicos Islands', '1-649'),
('Tuvalu', '688'),
('Uganda', '256'),
('Ukraine', '380'),
('United Arab Emirates', '971'),
('United Kingdom', '44'),
('United States of America', '1'),
('US Virgin Islands', '1-340'),
('Universal Personal Telecommunications (UPT)', '878'),
('Uruguay', '598'),
('Uzbekistan', '998'),
('Vanuatu', '678'),
('Vatican City', '39'),
('Venezuela', '58'),
('Vietnam', '84'),
('Wake Island', '808');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `user_code` varchar(10) NOT NULL,
  `speaker` int(11) DEFAULT '0',
  `answered` int(11) DEFAULT '0',
  `eventname` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_phone`, `user_question`, `asked_at`, `user_code`, `speaker`, `answered`, `eventname`) VALUES
(2, 'Javid', '8861303300', 'is it possible to enhance the quality of the slide deck since text isn\'t visible for reading', '2020-03-31 11:07:26', '91', 0, 0, 'cims'),
(3, 'Dr. Devang Sarvaiya', '9004893930', 'Please re-start the presentation', '2020-03-31 11:08:54', '91', 0, 0, 'cims'),
(4, 'Dr. Devang Sarvaiya', '9004893930', 'Keep 1st slide ready', '2020-03-31 11:09:14', '91', 0, 0, 'cims'),
(5, 'Dr. Devang Sarvaiya', '9004893930', 'Pawan bhai re-open slides', '2020-03-31 11:10:08', '91', 0, 0, 'cims'),
(6, 'Pooja', '9768161921', 'Audio & Video clear.... can see slide transitions', '2020-03-31 11:18:11', '91', 0, 0, 'cims'),
(7, 'Dr. Devang Sarvaiya', '9004893930', 'Please keep it on', '2020-03-31 11:23:16', '91', 0, 0, 'cims'),
(8, 'Dr. Devang Sarvaiya', '9004893930', 'Please restart', '2020-03-31 11:32:18', '91', 0, 0, 'cims'),
(9, 'Javid', '8861303300', 'CAN YOU PLEASE GO LIVE', '2020-03-31 11:34:34', '91', 0, 0, 'cims'),
(10, 'Payel Mukherjee ', '7622012610', 'hi', '2020-03-31 11:36:08', '91', 0, 0, 'cims'),
(11, 'Payel Mukherjee ', '7622012610', 'hi', '2020-03-31 11:36:58', '91', 0, 0, 'cims'),
(12, 'Javid', '8861303300', 'thank you team', '2020-03-31 11:41:02', '91', 0, 0, 'cims'),
(13, 'Payel Mukherjee ', '7622012610', 'hi', '2020-03-31 11:52:35', '91', 0, 0, 'cims'),
(14, 'Payel Mukherjee ', '7622012610', 'this is payel', '2020-03-31 11:52:53', '91', 0, 0, 'cims'),
(15, 'Payel Mukherjee ', '7622012610', 'hi somarrita', '2020-03-31 11:55:24', '91', 0, 0, 'cims'),
(16, 'Payel Mukherjee ', '7622012610', 'how is that speaker will view the question\r\nwill we get a repository of questions', '2020-03-31 11:57:10', '91', 0, 0, 'cims'),
(17, 'hemelin', '9769550543', 'Hello', '2020-04-07 11:27:11', '91', 0, 0, 'cims'),
(18, 'Raees', '9867087868', 'Hello', '2020-04-07 11:35:32', '91', 0, 0, 'cims'),
(19, 'Devang Sarvaiya', '9004893930', 'Please stream live', '2020-04-07 11:43:55', '91', 0, 0, 'cims'),
(20, 'Raees', '9867087868', 'Hello \r\ni\'m I audible?', '2020-04-07 11:46:37', '91', 0, 0, 'cims'),
(21, 'Raees', '9867087868', 'can we have interactive session?', '2020-04-07 11:46:59', '91', 0, 0, 'cims'),
(22, 'hemelin', '9769550543', 'Hello everyone', '2020-04-07 11:48:41', '91', 0, 0, 'cims'),
(23, 'Tania ', '9819140706', 'Can the audience see each other', '2020-04-07 11:49:32', '91', 0, 0, 'cims');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(4) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `cntry_code` varchar(10) DEFAULT NULL,
  `mobile_num` varchar(12) DEFAULT NULL,
  `location` varchar(100) NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) DEFAULT '0',
  `joining_date` datetime DEFAULT NULL,
  `eventname` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `cntry_code`, `mobile_num`, `location`, `login_date`, `logout_date`, `logout_status`, `joining_date`, `eventname`) VALUES
(3, 'PAWAN Shilwant', '93', '123456', 'Pune ', '2020-03-30 23:18:02', '2020-03-30 23:18:10', 0, '2020-03-30 23:18:02', 'cims'),
(4, 'Pooja', '91', '9768161921', 'Mumbai ', '2020-04-07 11:53:57', '2020-04-07 12:08:09', 1, '2020-03-31 09:37:25', 'cims'),
(5, 'Devang Sarvaiya', '91', '9004893930', 'India', '2020-04-07 10:41:46', '2020-04-07 12:48:16', 1, '2020-03-31 10:24:57', 'cims'),
(6, 'Javid', '91', '8861303300', 'bangalore', '2020-03-31 17:03:45', '2020-03-31 18:25:46', 1, '2020-03-31 10:25:05', 'cims'),
(7, 'Sujatha ', '91', '9845563760', 'Coact bangalore ', '2020-04-07 12:12:38', '2020-04-07 12:31:35', 1, '2020-03-31 10:57:33', 'cims'),
(8, 'Akshat', '91', '7204420017', 'Bangalore ', '2020-04-07 11:48:25', '2020-04-07 12:17:40', 1, '2020-03-31 11:05:57', 'cims'),
(9, 'Somarrita', '91', '9988889772', 'Hyderabad', '2020-03-31 11:33:13', '2020-03-31 11:37:19', 0, '2020-03-31 11:10:27', 'cims'),
(10, 'Payel Mukherjee ', '91', '7622012610', 'hyderabad', '2020-03-31 11:55:14', '2020-03-31 11:57:52', 0, '2020-03-31 11:32:22', 'cims'),
(11, 'PAWAN', '91', '09545329222', 'Bangalore', '2020-04-07 11:30:45', '2020-04-07 12:54:16', 1, '2020-03-31 11:46:55', 'cims'),
(12, 'Swapnil V', '91', '9967330403', 'Mumbai', '2020-04-07 11:30:37', '2020-04-07 11:48:08', 1, '2020-04-07 10:47:48', 'cims'),
(13, 'Ravi Khot', '91', '9765875731', 'Pune', '2020-04-07 11:09:11', '2020-04-07 11:10:30', 1, '2020-04-07 11:09:11', 'cims'),
(14, 'Mahesh', '91', '0000011111', 'Mumbai', '2020-04-07 11:36:08', '2020-04-07 11:37:49', 0, '2020-04-07 11:25:40', 'cims'),
(15, 'SHUVANKAR ROY', '91', '9820138291', 'MUMBAI', '2020-04-07 11:46:19', '2020-04-07 16:36:40', 1, '2020-04-07 11:25:47', 'cims'),
(16, 'Anarghya', '91', '9538289001', 'Bengaluru', '2020-04-07 11:26:16', '2020-04-07 11:36:56', 1, '2020-04-07 11:26:16', 'cims'),
(17, 'hemelin', '91', '9769550543', 'Mumbai', '2020-04-07 11:47:39', '2020-04-07 12:00:01', 1, '2020-04-07 11:26:57', 'cims'),
(18, 'AKJ', '91', '987785', 'hg', '2020-04-07 11:28:09', '2020-04-07 11:30:54', 1, '2020-04-07 11:28:09', 'cims'),
(19, 'Ravi Khot', '91', '638', 'Hsj', '2020-04-07 11:30:28', '2020-04-07 11:33:14', 1, '2020-04-07 11:30:28', 'cims'),
(20, 'Ahmed', '91', '9995160003', 'Kerala', '2020-04-07 11:34:29', '2020-04-07 11:51:15', 1, '2020-04-07 11:34:29', 'cims'),
(21, 'Raees', '91', '9867087868', 'Mumbai', '2020-04-07 11:35:04', '2020-04-07 13:03:05', 1, '2020-04-07 11:35:04', 'cims'),
(22, 'Mahesh', '91', '0000011112', 'Mumbai', '2020-04-07 11:43:32', '2020-04-07 13:44:33', 1, '2020-04-07 11:43:32', 'cims'),
(23, 'Tania ', '91', '9819140706', 'Mumbai ', '2020-04-07 11:45:13', '2020-04-07 11:49:47', 1, '2020-04-07 11:45:13', 'cims'),
(24, 'P', '91', '9545329222', 'Pune ', '2020-04-07 11:46:20', '2020-04-07 11:47:20', 1, '2020-04-07 11:46:20', 'cims'),
(25, 'Vrunda ', '91', '9873679734', 'Mumbai', '2020-04-07 11:47:34', '2020-04-07 11:57:10', 1, '2020-04-07 11:47:34', 'cims'),
(26, 'Arun', '-', '-', '-', '2020-04-21 11:53:44', '2020-04-21 11:54:14', 1, '2020-04-21 11:53:44', 'cims'),
(27, 'Ramchandra', '-', '-', '-', '2020-04-21 12:46:42', '2020-04-21 12:47:12', 1, '2020-04-21 12:46:42', 'cims'),
(28, '', '-', '-', '-', '2020-04-21 15:14:17', '2020-04-21 15:14:47', 1, '2020-04-21 15:14:17', 'cims'),
(29, 'Steve', '-', '-', '-', '2020-04-21 15:16:33', '2020-04-21 15:17:03', 1, '2020-04-21 15:16:33', 'cims'),
(30, 'asdrg', '-', '-', '-', '2020-04-24 20:18:28', '2020-04-24 20:18:58', 1, '2020-04-24 20:18:28', 'cims'),
(31, 'sfs', '-', '-', '-', '2020-04-25 14:41:41', '2020-04-25 14:42:11', 1, '2020-04-25 14:41:41', 'cims'),
(32, 'Manish Singh', '-', '-', '-', '2020-04-25 15:35:34', '2020-04-25 15:36:04', 1, '2020-04-25 15:35:34', 'cims'),
(33, 'Pooja ', '-', '-', '-', '2020-05-12 16:19:13', '2020-05-12 16:19:43', 1, '2020-05-12 16:19:13', 'cims');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
